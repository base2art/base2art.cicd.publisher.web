﻿using System;

namespace Base2art.CiCd.Publisher.Extension.Nuget
{
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Models;
    using NuGet.Common;
    using NuGet.Protocol;
    using NuGet.Protocol.Core.Types;

    public class NugetPackageHandler : ArtifactHandlerBase
    {
        private readonly string pushUrl;
        private readonly string apiKey;

        public NugetPackageHandler(string pushUrl, string apiKey) : base("*.nupkg")
        {
            this.pushUrl = pushUrl;
            this.apiKey = apiKey;
        }

        // curl -X POST https://demo.octopus.com/api/packages/raw -H "X-Octopus-ApiKey: API-YOURAPIKEY" -F "data=@Demo.1.0.0.zip"
        protected override async Task HandleFile(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData artifact, TextWriter output)
        {
            ILogger logger = NullLogger.Instance;
            CancellationToken cancellationToken = CancellationToken.None;

            SourceRepository repository = Repository.Factory.GetCoreV3(this.pushUrl);
            var resx = await repository.GetResourceAsync<PackageUpdateResource>(cancellationToken);
            var path = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid():N}.nupkg");
            File.WriteAllBytes(path, artifact.Content);
            await resx.Push(
                            path,
                            null,
                            600*5,
                            false,
                            s => this.apiKey,
                            s => null,
                            false,
                            true,
                            null,
                            logger);

        }
    }
}

/*
 * 
            // repo.
            // curl --verbose -k -X PUT http://localhost:44387/nuget -H "X-NuGet-ApiKey: 1234567890abcd" -F "data=@C:\Path\To\Package.1.2.3.nupkg"
            // dotnet nuget push AppLogger.1.0.0.nupkg --api-key qz2jga8pl3dvn2akksyquwcs9ygggg4exypy3bhxy6w6x6 --source https://api.nuget.org/v3/index.json
 */