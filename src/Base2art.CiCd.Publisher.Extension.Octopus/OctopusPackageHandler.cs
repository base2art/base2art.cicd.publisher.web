﻿namespace Base2art.CiCd.Publisher.Extension.Octopus
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using Models;

    public class OctopusPackageHandler : ArtifactHandlerBase
    {
        private readonly string pushUrl;
        private readonly string apiKey;
        private readonly string newExtension;
        private static HttpClient backingClient;

        public OctopusPackageHandler(string pushUrl, string apiKey, string packageWildcard, string newExtension)
            : base(string.IsNullOrWhiteSpace(packageWildcard) ? "*.webapp" : packageWildcard)
        {
            this.pushUrl = pushUrl;
            this.apiKey = apiKey;
            this.newExtension = newExtension;
        }

        private HttpClient Client => backingClient ?? (backingClient = new HttpClient {Timeout = TimeSpan.FromHours(1)});

        protected override async Task HandleFile(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData artifact, TextWriter output)
        {
            // using (var client = new HttpClient())

            // var client = this.Client;
            using (var formContent = new MultipartFormDataContent())
            {
                var streamContent = new ByteArrayContent(artifact.Content);
                //File.WriteAllBytes("fake.zip", artifact.Content);
                streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                var newName = string.IsNullOrWhiteSpace(this.newExtension)
                                  ? artifact.Name
                                  : Path.ChangeExtension(artifact.Name, this.newExtension);

                formContent.Add(streamContent, "file", newName); //artifact.FileName);\

                using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, this.pushUrl))
                {
                    requestMessage.Method = HttpMethod.Post;
                    requestMessage.Headers.Add("X-NuGet-ApiKey", this.apiKey);
                    requestMessage.Content = formContent;

                    using (var response = await this.Client.SendAsync(requestMessage))
                    {
                        string input = await response.Content.ReadAsStringAsync();

                        await output.WriteLineAsync(input);
                        Console.WriteLine(input);
                    }
                }
            }
        }
    }
}