namespace Base2art.CiCd.Publisher.Web
{
    using System;
    using System.Threading.Tasks;
    using Resources;

    public interface IStateHandler
    {
        Task<bool> ContainsKey(Guid publishEventId);

        // ProjectPublish this[Guid publishEventId] { get; }
        Task Add(ProjectPublish projectPublish);
        Task<ProjectPublish> Get(Guid publishEventId);
        Task SetError(Guid newGuid, string output, string errorText);
        Task SetWorking(Guid newGuid, string output);
        Task SetSuccess(Guid taskId, string output, string errorText);
    }
}