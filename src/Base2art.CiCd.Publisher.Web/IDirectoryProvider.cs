namespace Base2art.CiCd.Publisher.Web
{
    using System.IO;

    public interface IDirectoryProvider
    {
        DirectoryInfo GetWorkingDir();
    }
}