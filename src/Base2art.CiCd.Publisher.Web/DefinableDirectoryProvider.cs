namespace Base2art.CiCd.Publisher.Web
{
    using System;
    using System.IO;

    public class DefinableDirectoryProvider : IDirectoryProvider
    {
        private readonly DirectoryInfo info;

        public DefinableDirectoryProvider(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || path.StartsWith("#"))
            {
                path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art/cicd/publish-node");
            }

            this.info = Directory.CreateDirectory(Path.Combine(path, "wd"));
        }

        public DirectoryInfo GetWorkingDir() => this.info;
    }
}