namespace Base2art.CiCd.Publisher.Web
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using Resources;

    public class FsStateHandler : IStateHandler
    {
        private readonly IDirectoryProvider directoryProvider;

        public FsStateHandler(IDirectoryProvider directoryProvider)
            => this.directoryProvider = directoryProvider;

        public Task<bool> ContainsKey(Guid publishEventId)
            => Task.FromResult(Directory.Exists(PathOf(publishEventId)));

        public Task Add(ProjectPublish projectPublish)
        {
            File.WriteAllText(Path.Combine(PathOf(projectPublish.Id), "log.out"), projectPublish.Output ?? "");
            File.WriteAllText(Path.Combine(PathOf(projectPublish.Id), "log.err"), projectPublish.Error ?? "");
            File.WriteAllText(Path.Combine(PathOf(projectPublish.Id), "log.state"), projectPublish.State.ToString("G"));
            File.WriteAllText(Path.Combine(PathOf(projectPublish.Id), "completed.date"), projectPublish.CompletedAt.ToString());

            return Task.CompletedTask;
        }

        public Task<ProjectPublish> Get(Guid newGuid)
        {
            var publish = new ProjectPublish();
            publish.Id = newGuid;
            publish.Output = ReadAllText(Path.Combine(PathOf(newGuid), "log.out"));
            publish.Error = ReadAllText(Path.Combine(PathOf(newGuid), "log.err"));

            Enum.TryParse<PublishPhaseState>(ReadAllText(Path.Combine(PathOf(newGuid), "log.state")), out var state);
            publish.State = state;

            DateTime? dt = null;

            if (DateTime.TryParse(
                                  ReadAllText(Path.Combine(PathOf(newGuid), "completed.date")),
                                  CultureInfo.InvariantCulture,
                                  DateTimeStyles.AssumeUniversal,
                                  out var result))
            {
                dt = result;
            }
            
            publish.CompletedAt = dt;

            return Task.FromResult(publish);
        }

        private string ReadAllText(string combine)
        {
            if (File.Exists(combine))
            {
                return File.ReadAllText(combine).Trim();
            }

            return string.Empty;
        }

        public Task SetError(Guid newGuid, string output, string errorText)
        {
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.out"), output ?? "");
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.err"), errorText ?? "");
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.state"), PublishPhaseState.CompletedFail.ToString("G"));
            File.WriteAllText(Path.Combine(PathOf(newGuid), "completed.date"), DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));

            return Task.CompletedTask;
        }

        public Task SetWorking(Guid newGuid, string toString)
        {
            // File.WriteAllText(Path.Combine(PathOf(newGuid), "log.out"), output ?? "");
            // File.WriteAllText(Path.Combine(PathOf(newGuid), "log.err"), errorText ?? "");
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.state"), PublishPhaseState.Working.ToString("G"));
            return Task.CompletedTask;
        }

        public Task SetSuccess(Guid newGuid, string output, string errorText)
        {
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.out"), output ?? "");
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.err"), errorText ?? "");
            File.WriteAllText(Path.Combine(PathOf(newGuid), "log.state"), PublishPhaseState.CompletedSuccess.ToString("G"));
            File.WriteAllText(Path.Combine(PathOf(newGuid), "completed.date"), DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));

            return Task.CompletedTask;
        }

        private string PathOf(Guid publishEventId)
            => Directory.CreateDirectory(Path.Combine(this.directoryProvider.GetWorkingDir().FullName, publishEventId.ToString("N"))).FullName;
    }
}