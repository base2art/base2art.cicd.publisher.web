namespace Base2art.CiCd.Publisher.Web.Public.Resources
{
    public class PublishHandlerData
    {
        public string SupportedExtension { get; set; }
        public bool RequireArtifactContent { get; set; }
    }
}