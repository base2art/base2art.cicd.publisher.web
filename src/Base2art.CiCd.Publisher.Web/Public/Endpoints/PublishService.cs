﻿namespace Base2art.CiCd.Publisher.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Extension;
    using Extension.Models;
    using Publisher.Resources;
    using Resources;
    using Servers;

    public class PublishService : IPublishPhaseService
    {
        private readonly IHandler[] handlers;

        private readonly IRequireAuthProvider auth;


        public PublishService(
            IHandler[] handlers,
            IRequireAuthProvider auth,
            IStateHandler backingState)
        {
            this.handlers = handlers ?? new IHandler[0];
            this.auth = auth;
            this.State = backingState;
        }

        protected IStateHandler State { get; }

        public Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal)
        {
            this.auth.RequireSignedIn(principal);

            var items = this.handlers.Select(x => new SupportedFeature
                                                  {
                                                      Id = x.Id,
                                                      FullName = x.FullName,
                                                      Name = x.Name,
                                                      DefaultData = new PublishHandlerData
                                                                    {
                                                                        SupportedExtension = x.SupportedExtension,
                                                                        RequireArtifactContent = x.RequireArtifactContent
                                                                    }
                                                  });

            return Task.FromResult(items.ToArray());
        }

        public async Task<ProjectPublish> GetPublishStatus(Guid publishEventId, ClaimsPrincipal principal)
        {
            if (await this.State.ContainsKey(publishEventId))
            {
                return await this.State.Get(publishEventId);
            }

            return new ProjectPublish
                   {
                       Id = publishEventId,
                       Output = "",
                       Error = "",
                       State = PublishPhaseState.Unknown,
                       CompletedAt = null
                   };
        }

        public async Task<ProjectPublish> StartPublish(Guid handlerId, PublishEvent eventDetails, ClaimsPrincipal principal)
        {
            if (eventDetails == null)
            {
                throw new ArgumentNullException(nameof(eventDetails));
            }

            this.auth.RequireSignedIn(principal);

            var newGuid = Guid.NewGuid();
            await this.State.Add(new ProjectPublish
                                 {
                                     Id = newGuid,
                                     Output = "",
                                     Error = "",
                                     State = PublishPhaseState.Pending,
                                     CompletedAt = null
                                 });

            Guid jobId = eventDetails.JobId;
            Project project = eventDetails.Project;
            BranchData[] branches = eventDetails.Branches;
            FileData[] artifacts = eventDetails.Artifacts;


            var handler = this.handlers.FirstOrDefault(x => x.Id == handlerId);
            if (handler == null)
            {
                await this.State.SetError(newGuid, "", "Cannot Find Handler");
                return await this.State.Get(newGuid);
            }

            await this.State.SetWorking(newGuid, "");
            var output = new CustomStringWriter(newGuid, this.State);
            var task = handler.Handle(
                                      jobId,
                                      this.Map(project),
                                      branches.Select(Map2).ToArray(),
                                      artifacts.Select(Map3).ToArray(),
                                      output);

            this.RunAway(newGuid, task, output);

            return await this.State.Get(newGuid);
        }

        private async void RunAway(Guid taskId, Task task, StringWriter output)
        {
            try
            {
                await this.RunAwaySync(taskId, task, output);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private async Task RunAwaySync(Guid taskId, Task task, StringWriter output)
        {
            try
            {
                await task;

                await this.State.SetSuccess(taskId, this.FormatException(task.Exception), Output(output));
            }
            catch (Exception)
            {
                await this.State.SetError(taskId, Output(output), this.FormatException(task.Exception));
                throw;
            }
        }

        private static string Output(StringWriter output)
        {
            output.FlushAsync();
            return output.GetStringBuilder().ToString();
        }

        private string FormatException(AggregateException taskException)
        {
            if (taskException == null)
            {
                return string.Empty;
            }

            var innerException = taskException.InnerExceptions;
            if (innerException.Count == 0)
            {
                return string.Empty;
            }

            var parts = innerException.Select(x => $"{x.Message}{Environment.NewLine}{x.StackTrace}");

            return string.Join(Environment.NewLine + Environment.NewLine, parts);
        }

        private FileMetaData Map3(FileData project)
            => new FileMetaData
               {
                   Content = project.Content,
                   RelativeName = project.RelativeName,
                   Name = project.Name
               };

        private BranchMetaData Map2(BranchData project)
            => new BranchMetaData
               {
                   TypeId = project.TypeId,
                   Hash = project.Hash,
                   Id = project.Id,
                   Name = project.Name
               };

        private ProjectData Map(Project project)
            => new ProjectData
               {
                   Id = project.Id,
                   Name = project.Name
               };

        private class CustomStringWriter : StringWriter
        {
            private readonly Guid newId;
            private readonly IStateHandler handler;

            public CustomStringWriter(Guid newId, IStateHandler handler) : base()
            {
                this.newId = newId;
                this.handler = handler;
            }
            public override Task FlushAsync()
            {
                this.handler.SetWorking(this.newId, this.GetStringBuilder().ToString());
                return base.FlushAsync();
            }
        }
    }

}