namespace Base2art.CiCd.Publisher.Web
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Resources;

    public class InMemoryStateHandler : IStateHandler
    {
        private static readonly Dictionary<Guid, ProjectPublish> Item = new Dictionary<Guid, ProjectPublish>();

        public Task<bool> ContainsKey(Guid publishEventId)
            => Task.FromResult(Item.ContainsKey(publishEventId));

        public Task Add(ProjectPublish projectPublish)
        {
            Item[projectPublish.Id] = projectPublish;
            return Task.CompletedTask;
        }

        public Task<ProjectPublish> Get(Guid publishEventId)
            => Task.FromResult(Item[publishEventId]);

        public Task SetError(Guid newGuid, string output, string errorText)
        {
            Item[newGuid].Output = output;
            Item[newGuid].Error = errorText;
            Item[newGuid].State = PublishPhaseState.CompletedFail;
            return Task.CompletedTask;
        }

        public Task SetWorking(Guid newGuid, string toString)
        {
            Item[newGuid].State = PublishPhaseState.Working;
            return Task.CompletedTask;
        }

        public Task SetSuccess(Guid taskId, string output, string errorText)
        {
            Item[taskId].Output = output;
            Item[taskId].Error = errorText;
            Item[taskId].State = PublishPhaseState.CompletedSuccess;
            return Task.CompletedTask;
        }
    }
}