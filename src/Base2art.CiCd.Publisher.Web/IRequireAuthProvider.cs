namespace Base2art.CiCd.Publisher.Web
{
    public interface IRequireAuthProvider
    {
        bool Value { get; }
    }
}