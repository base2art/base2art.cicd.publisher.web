// namespace Base2art.CiCd.Publisher.Extension
// {
//     using System;
//     using System.IO;
//     using System.Threading.Tasks;
//     using Models;
//
//     public abstract class PushHandlerBase : IHandler
//     {
//         public virtual Guid Id => this.FullName.HashAsGuid();
//         public virtual string Name => this.GetType().Name;
//         public virtual string FullName => this.GetType().FullName;
//         public string SupportedExtension { get; } = null;
//         public bool RequireArtifactContent { get; } = false;
//
//         async Task IHandler.Handle(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData[] artifacts, TextWriter outputWriter)
//         {
//             if (string.IsNullOrWhiteSpace(this.SupportedExtension))
//             {
//                 await outputWriter.WriteLineAsync("push handling by... " + this.GetType());
//                 foreach (var artifact in artifacts)
//                 {
//                     await outputWriter.WriteLineAsync("With " + artifact.Name);
//                 }
//
//                 await this.HandleArtifacts(jobId, project, branches, artifacts, outputWriter);
//             }
//         }
//
//         protected abstract Task HandleArtifacts(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData[] artifacts, TextWriter outputWriter);
//     }
// }