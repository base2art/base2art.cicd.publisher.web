namespace Base2art.CiCd.Publisher.Extension.Models
{
    using System;

    public class ProjectData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}