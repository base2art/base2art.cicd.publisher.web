namespace Base2art.CiCd.Publisher.Extension.Models
{
    public class FileMetaData
    {
        public string Name { get; set; }
        public string RelativeName { get; set; }
        public byte[] Content { get; set; }
    }
}