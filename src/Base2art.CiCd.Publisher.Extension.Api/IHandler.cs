﻿namespace Base2art.CiCd.Publisher.Extension
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Models;

    public interface IHandler
    {
        string FullName { get; }

        Guid Id { get; }

        string Name { get; }

        string SupportedExtension { get; }

        bool RequireArtifactContent { get; }

        Task Handle(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData[] artifacts, TextWriter output);
    }
}