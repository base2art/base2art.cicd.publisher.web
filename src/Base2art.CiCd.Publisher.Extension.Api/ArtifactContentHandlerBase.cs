// namespace Base2art.CiCd.Publisher.Extension
// {
//     using System;
//     using System.Security.Claims;
//     using System.Threading.Tasks;
//
//     public abstract class ArtifactContentHandlerBase : IHandler
//     {
//         protected ArtifactContentHandlerBase(string supportedExtension)
//         {
//             this.SupportedExtension = supportedExtension;
//         }
//
//         public Guid Id => this.FullName.HashAsGuid();
//         public string Name => this.GetType().Name;
//         public string FullName => this.GetType().FullName;
//         public string SupportedExtension { get; }
//         public bool RequireArtifactContent { get; } = true;
//         
//         public Task Handle(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData[] artifacts, ClaimsPrincipal principal) 
//             => throw new NotImplementedException();
//     }
// }