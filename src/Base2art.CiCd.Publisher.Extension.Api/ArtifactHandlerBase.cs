namespace Base2art.CiCd.Publisher.Extension
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;

    public abstract class ArtifactHandlerBase : IHandler
    {
        protected ArtifactHandlerBase(string supportedExtension)
            => this.SupportedExtension = string.IsNullOrWhiteSpace(supportedExtension) ? "*" : supportedExtension;

        public Guid Id => this.FullName.HashAsGuid();
        public string Name => this.GetType().Name;
        public string FullName => this.GetType().FullName;
        public string SupportedExtension { get; }
        public bool RequireArtifactContent { get; } = false;

        Task IHandler.Handle(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData[] artifacts, TextWriter output)
        {
            var matchingArtifacts = artifacts.Where(artifact => this.SupportedExtension.IsWildCardMatch(artifact.RelativeName, true)).ToArray();
            var tasks = matchingArtifacts.Select(async artifact =>
            {
                await output.WriteLineAsync("push handling by... " + this.GetType());
                await this.HandleFile(jobId, project, branches, artifact, output);
                await output.WriteLineAsync("push handled by... " + this.GetType());
            }).ToArray();

            return Task.WhenAll(tasks);
        }

        protected abstract Task HandleFile(Guid jobId, ProjectData project, BranchMetaData[] branches, FileMetaData artifact, TextWriter output);
    }
}