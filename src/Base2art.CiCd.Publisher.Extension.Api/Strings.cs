namespace Base2art.CiCd.Publisher.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class Strings
    {
        private const StringComparison DefaultComparison = StringComparison.Ordinal;

        public static string AsString(this IEnumerable<char> chars) =>
            new string((chars ?? new char[0]).ToArray());

        public static string RemoveSuffix(this string value, string suffix) => value.RemoveSuffix(suffix, StringComparison.Ordinal);

        public static string RemoveSuffix(this string value, string suffix, StringComparison comp)
        {
            while (value.EndsWith(suffix, comp))
                value = value.Remove(value.Length - suffix.Length);
            return value;
        }

        public static string RemovePrefix(this string value, string prefix) => value.RemovePrefix(prefix, StringComparison.Ordinal);

        public static string RemovePrefix(this string value, string prefix, StringComparison comp)
        {
            while (value.StartsWith(prefix, comp))
                value = value.Substring(prefix.Length);
            return value;
        }

        public static string Join(this string[] values) => string.Join("", values);

        public static string Join(this IEnumerable<string> values) => string.Join("", values);

        public static string Join(this string[] values, string joiner) => string.Join(joiner, values);

        public static string Join(this IEnumerable<string> values, string joiner) => string.Join(joiner, values);

        public static bool StartsWithAny(this string value, IEnumerable<string> values) => value.StartsWithAnyInternal(values, StringComparison.Ordinal);

        public static bool StartsWithAny(
            this string value,
            StringComparison comp,
            IEnumerable<string> values)
        {
            return value.StartsWithAnyInternal(values, comp);
        }

        public static bool StartsWithAny(this string value, params string[] values) =>
            value.StartsWithAnyInternal((IEnumerable<string>) values, StringComparison.Ordinal);

        public static bool StartsWithAny(
            this string value,
            StringComparison comp,
            params string[] values)
        {
            return value.StartsWithAnyInternal((IEnumerable<string>) values, comp);
        }

        private static bool StartsWithAnyInternal(
            this string value,
            IEnumerable<string> values,
            StringComparison comp)
        {
            return values.Any(x => value.StartsWith(x, comp));
        }

        public static bool IsWildCardMatch(this string wildCard, string input, bool ignoreCase)
        {
            var wildCardToRegular = WildCardToRegular(wildCard, ignoreCase);
            return wildCardToRegular.IsMatch(input);
        }

        private static Regex WildCardToRegular(string value, bool ignoreCase)
        {
            string pattern = "^" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + "$";
            return ignoreCase ? new Regex(pattern, RegexOptions.IgnoreCase) : new Regex(pattern);
        }
    }
}