namespace Base2art.CiCd.Publisher.Web.Features
{
    using System.IO;

    public interface IDirectoryProvider
    {
        DirectoryInfo GetWorkingDir();
        DirectoryInfo DataDirectory();
    }
}